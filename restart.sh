#!/bin/sh

cd "$(dirname "$0")" || exit

docker-compose down
docker-compose up -d
